#include <stdio.h>
#include <string.h>
#include <stdlib.h>
// #include <ctype.h>

// #include "datatype_and_primitive.c"
#include "parser.c"

// Eval

struct enviroment{
  S* error;
  S* scopes;
};
typedef struct enviroment enviroment;

Boolean str_eq(char* str1, char* str2){
  return 0 == strcmp(str1, str2);
}

// functions

/*
S* foldr(S* (*func)(S*, S*), S* car, S* cdr){
  if (_is_nil(cdr)){
    return cdr;
  } else {
    return func(car,
                foldr(func, _car(cdr), _cdr(car)));
  }
}
*/

S* eval(S* symbol_tree, enviroment* env);

S* f_add(S* car, S* cdr){
  if (_is_nil(cdr) && _typeof(car) == t_int) {
    return car;
  } else if (_typeof(car) == t_int && _typeof(cdr) == t_cons_cell) {
    S* new_s = declare_S();
    new_s->_type = t_int;
    new_s->_value.int_value = \
      car->_value.int_value + f_add(_car(cdr), _cdr(cdr))->_value.int_value;
    return new_s;
  }
}

S* f_sub(S* car, S* cdr){
  if (_is_nil(cdr) && _typeof(car) == t_int) {
    return car;
  } else if (_typeof(car) == t_int && _typeof(cdr) == t_cons_cell) {
    S* new_s = declare_S();
    new_s->_type = t_int;
    new_s->_value.int_value = \
      car->_value.int_value - f_add(_car(cdr), _cdr(cdr))->_value.int_value;
    return new_s;
  }
}

S* f_mul(S* car, S* cdr){
  if (_is_nil(cdr) && _typeof(car) == t_int) {
    return car;
  } else if (_typeof(car) == t_int && _typeof(cdr) == t_cons_cell) {
    S* new_s = declare_S();
    new_s->_type = t_int;
    new_s->_value.int_value = \
      car->_value.int_value * f_add(_car(cdr), _cdr(cdr))->_value.int_value;
    return new_s;
  }
}

S* f_cons(S* car, S* cdr){
  S* value_S = declare_S();
  value_S = _cons(car, cdr);
  S* new_S = declare_S();
  new_S->_type = t_symbol;
  new_S->_value.symbol_points.symbols = value_S;
  return new_S;
}


S* f_list(S* car, S* cdr, enviroment* env){
  S* new_S = declare_S();
  if (_is_nil(cdr)) {
    new_S = _cons(car, cdr);
  } else {
    new_S = _cons(car, f_list(_car(cdr), _cdr(cdr), env));
  }
  return new_S;
}

S* f_cdr(S* car, S* cdr){
  if (car->_type == t_symbol){
    S* new_S = declare_S();
    new_S = _cdr(car->_value.symbol_points.symbols);
    return new_S;
  }
}

S* f_car(S* car, S* cdr){
  if (car->_type == t_symbol){
    S* new_S = declare_S();
    new_S = _car(car->_value.symbol_points.symbols);
    return new_S;
  }
}

// apply



S* apply_function (S* func_atom, S* argvs, enviroment* env){

  if(_typeof(func_atom) != t_variable){
    printf("symbol type error:");
    print_s(func_atom);
    printf("\n");
    return FALSE;
  }

  S* cdr_list = argvs;
  S* cdr_list_fst = cdr_list;
  while(!_is_nil(cdr_list)){
    
    S* tmp_tree = declare_S();
    tmp_tree = _car(cdr_list);
    //print_s(tmp_tree);

    *tmp_tree = *eval(tmp_tree, env);
    cdr_list = _cdr(cdr_list);
  }
  
  cdr_list = cdr_list_fst;

  //printf("ret-list: ");
  //print_s(cdr_list);
  //printf("\n");
  

  char* func_name=func_atom->_value.variable_points.name;

  ;;;;;; if (str_eq(func_name, "+")) {
    return f_add(_car(cdr_list), _cdr(cdr_list));
  } else if (str_eq(func_name, "-")) {
    return f_sub(_car(cdr_list), _cdr(cdr_list));
  } else if (str_eq(func_name, "*")) {
    return f_mul(_car(cdr_list), _cdr(cdr_list));
  } else if (str_eq(func_name, "cons")) {
    return f_cons(_car(cdr_list), _car(_cdr(cdr_list)));
  } else if (str_eq(func_name, "list")) {
    return f_list(_car(cdr_list), _cdr(_cdr(cdr_list)), env);
  } else if (str_eq(func_name, "car")) {
    return f_car(_car(cdr_list), _cdr(_cdr(cdr_list)));
  } else if (str_eq(func_name, "cdr")) {
    return f_cdr(_car(cdr_list), _cdr(_cdr(cdr_list)));
  } else {
    printf("error: not defined function:");
    print_s(func_atom);
    printf("\n");
  }
}


S* eval (S* symbol_tree, enviroment* env){
  if (_atomp(symbol_tree)){
    return symbol_tree;
  }

  if (_consp(symbol_tree)){
    S* tmp = declare_S();
    tmp = apply_function(_car(symbol_tree), _cdr(symbol_tree), env);
    
    return tmp;
  } else {
    printf("Error: data type\n");
    S* tmp = declare_S();
    *tmp = s_nil;
    return tmp;
  }

  return 0;
}


// REPL: Read Eval Print Loop

int Read_Eval_Print_Loop (S* enviroments){
  int iter = 0;
  S* formula_tree = declare_S();
  enviroment env;
  
  // S tmp = s_nil;
  while(1){
    printf("SS_%d > ", iter);
    getLine(stdin, enviroments);

    //print_s(enviroments);
    printf("\n");

    //S* stack=declare_S();
    //*stack = s_nil;
    formula_tree = char_list_to_S_formula_tree(enviroments);

    //print_s(formula_tree);
    
    while(!_is_nil(formula_tree)){
      print_s(_car(formula_tree));
      printf("\n");
      
      S* tmp_s = declare_S();
      tmp_s = eval(_car(formula_tree), &env);
      
      print_s(tmp_s);
      printf("\n");
      
      formula_tree = _cdr(formula_tree);
    }
    
    iter++;
  }
  return 0;
}


int main(){
  S* text_line = declare_S();
  
  Read_Eval_Print_Loop(text_line);
  
  return 0;
}

