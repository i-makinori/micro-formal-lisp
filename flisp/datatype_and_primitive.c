


// Data types

// S_cnr ::= atom p | (cons S_canr S_cdnr)

typedef struct S S;

// scope and garbage collection

struct trie_tree {
};

struct scope {
  // dynamic or lexical
};


struct _scoped_atom {
};


// atom

enum _origin_atom {
  nil, t, not,
  atomp, quote,
  cons, car, cdr,
  eq, cond,
  let, lambda
};

// symbol and name space

// typedef struct _symbol _symbol;

enum S_type {
  // cons_cell
  t_cons_cell,
  // atom
  t_origin_atom, t_symbol, t_int, t_char, t_variable};

union S_value {
  // as cons_cell
  struct {S* car; S* cdr;} _cons_cell;
  // as atom
  enum _origin_atom *origin_atom;
  struct {S* symbols;} symbol_points;
  struct {char* name; int name_length;} variable_points;
  int int_value;
  char char_value;
};

struct S {
  enum S_type _type;
  union S_value _value;
};


enum S_type _typeof(S* s){
  return s->_type;
}

// Constant

S s_nil = {t_origin_atom, nil};

// Verbs

typedef int Boolean;
#define TRUE 1
#define FALSE 0

Boolean _not(Boolean f){
  return (f==FALSE)? TRUE : FALSE;
}

Boolean _is_nil (S* s){
  return (s->_type == t_origin_atom) && (s->_value.origin_atom == nil);
}

Boolean _consp(S* s_formula){
  return (s_formula->_type == t_cons_cell)? TRUE : FALSE;
}

#define CATCH_RETURN_IF_NOT_CONS(S)             \
  if(!_consp((S))){                             \
    printf("S is not cons");                    \
    /* return Error */                          \
    return nil;                                 \
  };

S *_car(S* s_formula){
  if (_is_nil(s_formula)) {
    return &s_nil;
  }
  CATCH_RETURN_IF_NOT_CONS(s_formula);
  return (s_formula->_value._cons_cell).car;
  //return _valueof(s_formula).car;
}

S *_cdr(S* s_formula){
  if (_is_nil(s_formula)) {
    return &s_nil;
  }
  CATCH_RETURN_IF_NOT_CONS(s_formula);
  return (s_formula->_value._cons_cell).cdr;
}

Boolean _atomp(S* s_formula){
  return _not(_consp(s_formula));
}


Boolean _listp(S* s_formula) {
  // this function's source will be define of dolist.
  
  S* s_tmp = s_formula;
  while(!_is_nil(s_tmp)){
    // {Any x | (x is cons <=> x is not atom) && (x is not cons <=> x is atom)}.
    if (_atomp(s_tmp)) {
      return FALSE;
    }
    s_tmp = _cdr(s_tmp);
  }
  return TRUE;
  
  /*
    // this comment will be hint of tail call optimization
  if (_is_nil(s_formula)) {
    return TRUE;
  } else if (_consp(s_formula)) {
    return _listp(_cdr(s_formula));
  } else {
    return FALSE;
  }
  */
}


inline static S *declare_S(){
  S* s = (S*)malloc(sizeof(S));
  *s = s_nil;
  return s;
}

S *_cons(S* car, S* cdr){
  S* s = declare_S();
  //S s;
  s->_type = t_cons_cell;
  s->_value._cons_cell.car = car;
  s->_value._cons_cell.cdr = cdr;
  return s;
}


// print

int print_s(S* s_struct);

int print_s_value(enum S_type type, union S_value value){
  if(type == t_origin_atom) {
    printf("*ORIGIN*-%d", value);
  } else if (type == t_char) {
    printf("'%c'", value);
  } else if (type == t_int) {
    printf("%d", value);
  } else if (type == t_symbol) {
    //printf("'%s", value.symbol_points.symbols);
    printf("'");
    print_s(value.symbol_points.symbols);
  } else if (type == t_variable) {
    printf("%s", value.variable_points.name);
  } else {
    ;
  }
}

int print_s(S* s_struct){
  if (!_consp(s_struct)){
    print_s_value(s_struct->_type, s_struct->_value);
  } else if (_listp(s_struct)) {
    printf("(");
    S* s_tmp = s_struct;
    while (!_is_nil(s_tmp)) {
      print_s(_car(s_tmp));
      (!_is_nil(_cdr(s_tmp))) ? printf(" ") : nil;
      s_tmp = _cdr(s_tmp);
    }
    printf(")");
  } else if (s_struct->_type == t_cons_cell){
    printf("(");
    print_s(_car(s_struct));
    printf(" . ");
    print_s(_cdr(s_struct));
    printf(")");
  }
  return 0;
}

int print_type(S* s_struct){
  int type = s_struct->_type;
  char type_text[10];
  if(type == t_origin_atom) {
    strcpy(type_text, "origin");
  } else if (type == t_char) {
    strcpy(type_text, "char  ");
  } else if (type == t_int) {
    strcpy(type_text, "int   ");
  } else if (type == t_symbol) {
    strcpy(type_text, "symbol");
  } else if (type == t_variable) {
    strcpy(type_text, "variable");
  } else if (type == t_cons_cell) {
    strcpy(type_text, "cons_cell");
  } else {
    ;
  }
  printf("%s", type_text);
  return 0;
}
