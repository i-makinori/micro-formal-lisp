#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "datatype_and_primitive.c"


// SSection Read Eval Print Loop

S* getLine(FILE* stream, S* s_start){
  char c;
  S* s_current = s_start;

  if (s_start == NULL){
    printf("null is comming!!!\n");
    return nil;
  }
  while (((c = getc(stream)) != EOF) && (c != '\n')){

    // car: atom value
    S* p = declare_S();
    p->_type = t_char;
    p->_value.char_value = c;

    // cdr: s_next
    S* s_next = declare_S();

    // set to S_current's as cons
    *s_current = *_cons(p, s_next);

    // move pointer to cdr part
    s_current = s_next;
  }
  
  // last S as nil
  s_current->_type = t_origin_atom;
  s_current->_value.origin_atom = nil;
  
  return 0;
}


// Char list to S-formula syntax tree

#define CATCH_RETURN_IF_CAR_IS_NOT_CHAR(S)                              \
  if (_atomp(_car(current_pos)) && _typeof(_car((S))) != t_char){       \
    printf("not char list!!!\n");                                       \
    return nil;                                                         \
  };


Boolean char_is_member_of (char c, char* char_list){
  for(int iter=0; char_list[iter]!=0; iter++){
    if(c==char_list[iter]){
      return 1+iter;
    }
  }
  return FALSE;
}

#define CHAR_PAREN_START "("
#define CHAR_PAREN_END ")"

#define CHARS_DIGIT "0123456789"
#define CHARS_LOWER "abcdefghijklmnopqrstuvwxyz"
#define CHARS_UPPER "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define CHARS_SIGNS "!$%&*+,-./<=>?@^_|~"
// not "#'()[]{}:;\` , space, newline and endline
#define CHARS_FOR_QUOTE_NAME CHARS_DIGIT CHARS_LOWER CHARS_UPPER CHARS_SIGNS

Boolean is_white_space(char c){
  return (c==' '|| c=='\n' || c=='\r');
}

Boolean is_paren_start(char c){
  return (c=='(');
}

Boolean is_paren_end(char c){
  return (c==')');
}

Boolean is_delimiter(char c){
  return (is_white_space(c) || is_paren_start(c) || is_paren_end(c) ||
          c == '"' || c ==';');
}


// parsers

Boolean maybe_to_string(S* tmp_s, S* char_list);
#if 0
{
  char c;
  S* crr = char_list;
  int str_len = 0;
  
  c = _car(crr)->_value.char_value;
  if(c != '"'){
    return FALSE;
  }
  
  int digit_pos = 0;
  char tmp_str[SYMBOL_NAME_LENGTH_MAX];
  Boolean escaping = FALSE;

  while(!_is_nil(crr)){
    c = _car(crr)->_value.char_value;
    if (escaping==FALSE && c=='"'){
      tmp_s->_type = t_string;
      return str_len;
    } if (c=='\\')
  }
}
#endif



S* maybe_to_integer(S* tmp_s, S* char_list){
  char c;
  S* current = char_list;

  int digit_pos = 0;
  int tmp_num = 0;
  
  while(!_is_nil(current)){
    c = _car(current)->_value.char_value;
    if (is_delimiter(c)) {
      break;
    } else if (digit_pos = char_is_member_of(c, CHARS_DIGIT)) {
      tmp_num = tmp_num * 10 + (digit_pos-1);
    } else {
      current = char_list;
      break;
    }
    current = _cdr(current);
  }
  if (current == char_list){
    return char_list;
  }
  
  tmp_s->_type = t_int;
  tmp_s->_value.int_value = tmp_num;
  *char_list = *current;

  return current;
}

#define SYMBOL_NAME_LENGTH_MAX 255

S* maybe_to_variable(S* tmp_s, S* char_list){
  char c;
  S* current = char_list;
  int name_length = 0;
  
  int digit_pos = 0;
  char tmp_str[SYMBOL_NAME_LENGTH_MAX];
  memset(tmp_str, 0, sizeof(tmp_str));
  
  while(!_is_nil(current)){
    c = _car(current)->_value.char_value;
    
    if (is_delimiter(c)) {
      break;
    } else if (digit_pos = char_is_member_of(c, CHARS_FOR_QUOTE_NAME)) {
      tmp_str[name_length] = CHARS_FOR_QUOTE_NAME[digit_pos-1];
      name_length++;
    } else {
      break;
    }
    current = _cdr(current);
  }
  
  if (name_length == 0){
    return char_list;
  }
  
  char* name_str = (char*)malloc(sizeof(char) * (name_length+1));
  if(name_str==NULL){
    printf("error: failed to malloc string_to_symbol\n");
    exit(EXIT_FAILURE);
  }
  
  strcpy(name_str, tmp_str); // toto: map tolower(to_symbol_char) symbol_name::string
  
  tmp_s->_type = t_variable;
  tmp_s->_value.variable_points.name_length = strlen(tmp_str);
  tmp_s->_value.variable_points.name = name_str;
  
  *char_list = *current;
  return current;
}


S* maybe_to_symbol_tree (S* tmp_s, S* char_list, int* num_nest);
Boolean maybe_to_s_p (S* tmp_s, S* char_list, int* num_nest);

S* maybe_to_symbol(S* tmp_s, S* char_list, int* num_nest){

  char c0 = _car(char_list)->_value.char_value;
  if (c0 != '\'') {
    return char_list;
  } else if (c0 == '\'') {
    printf("error: quote syntax have some bugs.\n");
    S* crr = _cdr(char_list);
    S* crr_diff = crr;
    
    S* s_symbol = declare_S();
    
    if(maybe_to_s_p(s_symbol, crr, num_nest)){
    }
    
    print_s(s_symbol);
    printf("\n");
    
    tmp_s->_type = t_symbol;
    tmp_s->_value.symbol_points.symbols = s_symbol;
    
    *char_list = *crr;
    return crr;
  }

}

Boolean maybe_to_s_p (S* tmp_s, S* char_list, int* num_nest){

  if(_is_nil(char_list)){
    return TRUE;
  }

  char c = _car(char_list)->_value.char_value;

  while (is_white_space(c)){
    *char_list = *_cdr(char_list);
     c = _car(char_list)->_value.char_value;
  }
  S* current = char_list;
  S* diff_check = char_list;
  ;;;;;; if(char_list != maybe_to_symbol   (tmp_s, current, num_nest)) {
  } else if(char_list != maybe_to_integer  (tmp_s, current)){
  } else if(char_list != maybe_to_variable (tmp_s, current)){
  } else if(char_list != maybe_to_symbol_tree(tmp_s, current, num_nest)) {
    *tmp_s = *_car(tmp_s);
  } else {
    return FALSE;
  }

  *char_list = *current;
  return TRUE;
}

static void cons_next_list(S** tmp_s, S** list){
  S* next_s = declare_S();
  **list = *_cons(*tmp_s, next_s);
  
  *list = _cdr(*list);
  *tmp_s = declare_S();
}

S* maybe_to_symbol_tree (S* tmp_s, S* char_list, int* num_nest){
  
  S* list = declare_S();
  S* list_fst = list;

  S* tmp_s_c = declare_S();
  S* current_pos = char_list;
  char c;

  int num_nest_fst = *num_nest;

  while(!_is_nil(current_pos)){
    
    CATCH_RETURN_IF_NOT_CONS(current_pos);
    CATCH_RETURN_IF_CAR_IS_NOT_CHAR(current_pos);
    
    c = _car(current_pos)->_value.char_value;

    {;;;;} if(is_paren_end(c)) {
      current_pos = _cdr(current_pos);
      *num_nest = *num_nest-1;
      break;
    } else if(is_paren_start(c)) {
      current_pos = _cdr(current_pos);
      *num_nest = *num_nest+1;
      current_pos = maybe_to_symbol_tree(tmp_s_c, current_pos, num_nest);
      cons_next_list(&tmp_s_c, &list);
    } else if(is_white_space(c)) {
      current_pos = _cdr(current_pos);
    } else if(maybe_to_s_p(tmp_s_c, current_pos, num_nest)) {
      cons_next_list(&tmp_s_c, &list);
    } else {
      printf("syntax error: \n");
      print_s(char_list);
      printf("\n");
      return list_fst;
    }
    
    if(*num_nest<=0){
      break;
    }
  }

  *tmp_s = *list_fst;
  *char_list = *current_pos;
  return current_pos;
}


S* char_list_to_S_formula_tree (S* char_list){

  S* progn_list = declare_S();
  S* progn_list_car = progn_list;
  
  S* tmp_s = declare_S();
  int num_nest = 0;
  Boolean error = FALSE;

  S* formula_list = declare_S();
  
  //maybe_to_symbol_tree(tmp_s, char_list, &num_nest);
  
  while (!_is_nil(char_list)){
    num_nest = 0;
    maybe_to_s_p(tmp_s, char_list, &num_nest);
    
    if(num_nest!=0){
      puts("parren nest error:");
      print_s(tmp_s);
      printf("\n");
      error = TRUE;
      break;
    } else {
      cons_next_list(&tmp_s, &progn_list);
    }
  }

  if(error){
    return FALSE;
  } else {
    cons_next_list(&tmp_s, &progn_list);
    return progn_list_car;
  }
}
