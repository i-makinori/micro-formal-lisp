# micro-formal-lisp


```

(define func 
  (lambda (a b)
    (* a b)))

```


### References

- [Revised5 Report on the Algorithmic LanguageScheme](https://schemers.org/Documents/Standards/R5RS/r5rs.pdf)
- [Contents • Build Your Own Lisp](https://buildyourownlisp.com/contents)
- [rui314/minilisp: A readable lisp in less than 1k lines of C](https://github.com/rui314/minilisp)
