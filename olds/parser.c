
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ncurses.h>

// type := quote | function
// function := cons_cell
// cons_cell := (type car . type cdr)


// Snr = Atom x | Cons (Snar . Sndr)

typedef struct s S;

typedef enum origin_atom {
  nil, t,
  not,
  eq, atomp, quote,
  cons, car, cdr,
  cond,
  let, lambda
} origin_atom;

typedef union atom_value{
  int int_value;
  char char_value;
  origin_atom *origin_atom;
  S *s_quoted;
} atom_value;

typedef enum atom_type{
  origin, _int, _char, _s
} atom_type;

typedef struct atom {
  atom_type type;
  atom_value value;
} atom;

// S formula
struct s{
  atom *atom;
  S *s_car;
  S *s_cdr;
};


int getLine(FILE* stream, S* s_start){
  
  char c;
  S* s_current = s_start;
  
  if (s_start == NULL)
    return 1;
  while (((c = getc(stream)) != EOF) && (c != '\n')){
    // new S (car)
    S* s_current_car = (S*)malloc(sizeof(S));
    S* s_next = (S*)malloc(sizeof(S));
    
    // atom value
    atom *p = NULL;
    p = (atom*)malloc(sizeof(atom));
    p->type = _char;
    p->value.char_value = c;
    
    // set to S
    s_current_car->atom = p;
    s_current->s_car = s_current_car;
    s_current->s_cdr = s_next;

    s_current = s_next;
  }
  // nil last
  atom *p = NULL;
  p = (atom*)malloc(sizeof(atom));
  p->type = origin;
  p->value.origin_atom = nil;
  s_current->atom = p;

  //*buffer = '\0';
  return 0;
}

int print_atom(atom* p){
  //if(s_struct->atom->typ==c) {
  //atom* p = s_struct->atom;
  if(p->type==origin) {
    printf("*ORIGIN*-%d", p->value);
  } else if (p->type==_char) {
    printf("%c", p->value);
  } else if (p->type==_int) {
    printf("%d", p->value);
  } else if (p->type==_s) {
    printf("'");
    //print_s(p->value);
  }
  //printf("%c", s_struct->atom->value);
}

int print_s(S* s_struct){
  //if (s_struct->atom!=nil){
    //printf("nil");
  //}
  //atom_type type = s_struct->atom->type;
  if(s_struct->atom){
    print_atom(s_struct->atom);
  } else {
    printf("(");
    print_s(s_struct->s_car);
    printf(" . ");
    print_s(s_struct->s_cdr);
    printf(")");
  }
  return 0;
}


int main(){
  S text_line;
  getLine(stdin, &text_line);
  printf("%d\n", &text_line);
  print_s(&text_line);
  return 0;
}



/*
struct data_type{
  
};

char** string_to_element(char* str){
  int n_element = 0;
  int name_len = 0;
  char **element_name_list;
  int iter = 0;
  
  while(str[iter] == '\0'){
    while(str[iter] == ' '){
      element_name_list[n_element][name_len];
      iter++;
      name_len++;
    }
    name_len = 0;
    iter++;
  }

  return element_name_list;
}

void print_element_name_list(char** element_name_list){
  int i = 0;
  while(element_name_list[i] != NULL){
    printf("%s -> ", element_name_list[i]);
    i ++;
  }
}



int main(){

  int line_length=98;
  char line_text[line_length];
  int quit_p = nil;

  char **element_name_list;

  // Read Eval Print Loop
  // Loop
  while(quit_p == nil){
    //strcpy(line_text, "");
    *line_text = '\n';
    // read
    printf(">> ");
    //scanf("%s", line_text);
    fgets(line_text, sizeof line_text, stdin);


    if (strcmp(line_text, "quit") == 0) {
      quit_p = t;
    } else {
      element_name_list = string_to_element(line_text);
    }
    
    // Print
    printf("%s\n", line_text);
    print_element_name_list(element_name_list);

    // Loop
  }
  
  printf("Bye\n");

  return 0;
}

*/
